'use strict';

module.exports = function (grunt) {

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Configurable paths for the application
  var appConfig = {
    public: './public',
    dist: './dist'
  };

  grunt.initConfig({

    yeoman: appConfig,

    sass: {
      options: {
        sourceMap: true
      },
      dev: {
        files: { '<%= yeoman.public %>/css/styles.css': '<%= yeoman.public %>/scss/styles.scss'},
        options: {
          outputStyle: 'nested'
        }
      },
      dist: {
        files: { '<%= yeoman.public %>/css/styles.css': '<%= yeoman.public %>/scss/styles.scss'},
        options: {
          outputStyle: 'compressed'
        }
      }
    },

    watch: {
      js: {
        files: ['<%= yeoman.public %>/js/**/*.js'],
        options: {
          livereload: true
        },
        tasks: ['browserify']
      },
      html: {
        files: ['<%= yeoman.public %>/js/**/*.js'],
        options: {
          livereload: true
        },
        tasks: ['html2js']
      },
      css: {
        files: ['<%= yeoman.public %>/css/styles.css'],
        options: {
          livereload: true
        }
      },
      sass: {
        files: ['<%= yeoman.public %>/scss/**/*.scss'],
        tasks: ['sass:dev']
      },
      express: {
        files:  [ '<%= yeoman.public %>/app.js' ],
        tasks:  [ 'express:dev' ],
        options: {
          spawn: false
        }
      }
    },

    express: {
      dev: {
        options: {
          script: './app.js',
          node_env: 'development',
          port: 8085
        }
      }
    },

    clean: ['./tmp'],

    concat: {
      options: {
        sourceMap: false
      },
      dist: {
        src: ['./public/js/**/*.js', '!./public/js/app.*.js'],
        dest: './.tmp/app.concat.js'
      }
    },

    babel: {
      options: {
        sourceMaps: false
      },
      dist: {
        files: {
          './public/js/app.babel.js': ['./.tmp/app.concat.js']
        }
      }
    },

    uglify: {
      dist: {
        files: {
          './public/js/app.min.js': ['./public/js/app.babel.js']
        }
      }
    },

    browserify: {
      client: {
        src: ['public/js/index.js'],
        dest: 'public/dist/app-bundled.js',
        options: {
          'transform': [ 'browserify-shim' ],
          browserifyOptions: {
            debug: true
          }
        }
      }
    },

    html2js: {
      options: {
        singleModule: true,
        base: './public'
      },
      main: {
        src: ['./public/js/**/*.html'],
        dest: './public/dist/templates.js'
      },
      icons: {
        src: ['./public/icons/*'],
        dest: './public/dist/icons.js'
      }
    },
  });

  // grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {

  //   grunt.task.run([
  //     'build',
  //     'express:dev',
  //     'watch'
  //   ]);
  // });

  grunt.registerTask('t', ['ngtemplates', 'browserify']);
  grunt.registerTask('develop', ['html2js', 'browserify', 'watch']);
  // grunt.registerTask('js', ['clean', 'concat', 'babel', 'uglify']);
  // grunt.registerTask('build', ['sass:dev', 'js']);
};
