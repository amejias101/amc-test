require('angular/angular');

const common = angular.module('common', []);

common.factory('PlaylistsService', require('./playlists.service.js'));
common.controller('MainController', require('./main.ctrl'));
common.constant('config', require('./constants.js'));
module.exports = common;
