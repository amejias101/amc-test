module.exports = function (Slug, $mdToast, config, localStorageService) {
  const storageKey = 'playlists';
  const seedData = {
    'playlist-one': {
      createdAt: 1478307720900,
      slug: 'playlist-one',
      label: 'Playlist One',
      icon: 'communication-phone',
      iconUrl: false,
      songs: [ {
        'customImage': 'http://placehold.it/50/3498db',
        'track': 'Commodo et irure occaecat adipisicing.',
        'artist': 'Elit laboris dolore voluptate mollit in incididunt dolor dolor.',
        'album': 'Irure ipsum veniam id reprehenderit nostrud exercitation sunt officia deserunt.',
        'note': 'Eu do irure et ullamco laborum adipisicing nulla tempor aliquip sint cillum cillum do. Laborum excepteur veniam dolore Lorem fugiat duis deserunt exercitation.',
        'spotifyId': '5821602e9d0fa3c02bd67430',
        'spotifyUrl': 'http://abc.com'
      }, {
        'customImage': 'http://placehold.it/50/16a085',
        'track': 'Reprehenderit nulla ea dolor magna irure.',
        'artist': 'Ad aute proident cupidatat mollit non amet esse officia veniam ullamco minim exercitation veniam deserunt.',
        'album': 'Reprehenderit ut excepteur magna reprehenderit nisi aute et mollit aliquip velit consequat fugiat.',
        'note': 'Incididunt dolor ut aute id ullamco consequat est excepteur sit aliquip commodo sit consectetur consequat. Est quis dolore cillum sint dolor officia eu proident aliqua proident ea fugiat.',
        'spotifyId': '5821602efef7902e4bd1436e',
        'spotifyUrl': 'http://abc.com'
      }, {
        'customImage': 'http://placehold.it/50/2ecc71',
        'track': 'Eiusmod aliqua dolor ea eiusmod deserunt incididunt sint consectetur id.',
        'artist': 'Enim sint anim sit Lorem ullamco laborum labore dolore et consequat reprehenderit.',
        'album': 'Tempor irure in esse adipisicing incididunt incididunt nostrud deserunt.',
        'note': 'Nostrud veniam consectetur aute dolore excepteur officia amet. Sit ipsum aliquip adipisicing dolore in consectetur irure id qui.',
        'spotifyId': '5821602ee564f4e529c77447',
        'spotifyUrl': 'http://abc.com'
      }, {
        'track': 'Duis in cillum nisi pariatur non in irure deserunt.',
        'artist': 'Tempor non esse tempor deserunt sunt ipsum.',
        'album': 'Tempor nostrud consectetur nisi consectetur aute pariatur.',
        'note': 'Proident id anim proident commodo eiusmod est. Ullamco eiusmod laboris sit reprehenderit irure fugiat ut commodo reprehenderit magna do aute exercitation consequat.',
        'spotifyId': '5821602ea0e1f97b2594b9f2',
        'spotifyUrl': 'http://abc.com'
      }, {
        'customImage': 'http://placehold.it/50/2c3e50',
        'track': 'Reprehenderit amet dolore est ullamco.',
        'artist': 'Duis ullamco ut mollit elit deserunt eiusmod mollit qui velit et.',
        'album': 'Nulla et qui incididunt ex velit.',
        'note': 'Aute ea velit est consequat ea eiusmod pariatur reprehenderit. Commodo velit sit sit laborum cillum velit proident amet fugiat eiusmod ut.',
        'spotifyId': '5821602eb6933314f0d2857d',
        'spotifyUrl': 'http://abc.com'
      } ]
    },
    'playlist-two': {
      createdAt: 1478307720900,
      slug: 'playlist-two',
      label: 'Playlist Two',
      icon: 'communication-phone',
      iconUrl: false,
      songs: [{
        'track': 'Nostrud Lorem nisi minim velit.',
        'artist': 'Sunt labore exercitation Lorem occaecat sint nostrud elit sint adipisicing reprehenderit est.',
        'album': 'Consequat fugiat voluptate anim minim ea deserunt cupidatat.',
        'note': 'Laboris aliquip non do eiusmod nisi nulla dolor. Anim excepteur incididunt aliqua in officia sit voluptate id nostrud eiusmod aliqua irure magna.',
        'spotifyId': '5821602ee5e23521dfd43af8',
        'spotifyUrl': 'http://abc.com'
      }, {
        'track': 'Consectetur minim nostrud nulla non duis do ad laborum ex ex nostrud.',
        'artist': 'Dolore labore elit officia fugiat quis.',
        'album': 'Quis labore aliqua voluptate exercitation qui eu.',
        'note': 'Ad esse minim in elit ex non do excepteur enim. Anim occaecat sit sunt sit sint ut et nisi veniam reprehenderit elit exercitation ipsum sint.',
        'spotifyId': '5821602ee977cdd59bb5cc50',
        'spotifyUrl': 'http://abc.com'
      }, {
        'customImage': 'http://placehold.it/50/2ecc71',
        'track': 'Do pariatur enim pariatur sint exercitation fugiat reprehenderit ea dolore veniam irure Lorem occaecat.',
        'artist': 'Mollit esse dolor dolor ex magna voluptate voluptate.',
        'album': 'Et nisi sunt veniam nulla et tempor nulla laboris ex.',
        'note': 'Aute esse mollit aliqua Lorem ipsum consectetur do reprehenderit amet. Eu ipsum est officia eu ut consequat magna.',
        'spotifyId': '5821602e9cd0e256bca10756',
        'spotifyUrl': 'http://abc.com'
      }, {
        'customImage': 'http://placehold.it/50/2ecc71',
        'track': 'Ut aliquip non incididunt adipisicing reprehenderit qui dolor ipsum anim.',
        'artist': 'Commodo incididunt et laboris fugiat dolore velit reprehenderit.',
        'album': 'Cillum laboris aliqua eiusmod proident dolore ea eu fugiat amet Lorem ipsum aliquip.',
        'note': 'Adipisicing nostrud aliqua amet aliqua veniam quis enim. Laboris culpa adipisicing est non exercitation ea culpa consequat veniam eu aliqua officia.',
        'spotifyId': '5821602ef50a3daafb67efbd',
        'spotifyUrl': 'http://abc.com'
      }, {
        'customImage': 'http://placehold.it/50/2c3e50',
        'track': 'Consequat Lorem ut ex commodo aliqua exercitation voluptate aliquip Lorem pariatur veniam fugiat.',
        'artist': 'Qui exercitation nostrud aute anim ea.',
        'album': 'Ut sint et reprehenderit adipisicing est velit sint et aute amet ut aute ad aliquip.',
        'note': 'Voluptate anim et ullamco et dolor. Sit quis do magna exercitation culpa quis pariatur id.',
        'spotifyId': '5821602e7368f4d5a7603298',
        'spotifyUrl': 'http://abc.com'
      }, {
        'track': 'Do elit quis id aute ea.',
        'artist': 'In nostrud consequat et ex magna dolore proident nisi amet adipisicing duis magna pariatur.',
        'album': 'Irure consectetur officia id mollit.',
        'note': 'Aliquip sit pariatur laborum velit est velit enim id. Duis nisi exercitation ipsum dolore in ut nisi occaecat proident id.',
        'spotifyId': '5821602e7b5a065657fa5e8e',
        'spotifyUrl': 'http://abc.com'
      } ]
    },
    'playlist-three': {
      createdAt: 1478307720900,
      slug: 'playlist-three',
      label: 'Playlist Three',
      icon: 'communication-phone',
      iconUrl: false,
      songs: [{
        'customImage': 'http://placehold.it/50/3498db',
        'track': 'Ut elit pariatur qui adipisicing ex adipisicing do ad voluptate.',
        'artist': 'Aliqua non est quis esse ullamco sunt cillum.',
        'album': 'Labore voluptate laborum sint do exercitation ipsum ut amet.',
        'note': 'Est elit culpa culpa laborum exercitation labore excepteur enim nisi mollit qui duis. Aute velit adipisicing fugiat consequat velit.',
        'spotifyId': '5821602ea3d2de7c524f8a70',
        'spotifyUrl': 'http://abc.com'
      }, {
        'track': 'Reprehenderit reprehenderit aute excepteur proident consequat dolore voluptate occaecat tempor pariatur.',
        'artist': 'Laboris duis et magna officia ut.',
        'album': 'Officia incididunt minim nulla ipsum.',
        'note': 'Do proident occaecat labore nulla ut aliquip aliquip reprehenderit proident nulla adipisicing commodo ea. Lorem eiusmod incididunt aute ut veniam.',
        'spotifyId': '5821602e1b5dc37e3b21e2dc',
        'spotifyUrl': 'http://abc.com'
      }, {
        'customImage': 'http://placehold.it/50/3498db',
        'track': 'Aliqua ipsum magna dolore et do ad cupidatat esse ad.',
        'artist': 'Ad et Lorem excepteur exercitation deserunt.',
        'album': 'Reprehenderit cupidatat ex mollit sint esse sit aute nulla consectetur ex.',
        'note': 'Aliqua reprehenderit qui nulla consequat aute mollit deserunt labore reprehenderit voluptate aliquip nisi laborum officia. Eu excepteur excepteur qui consequat sunt.',
        'spotifyId': '5821602e78374814e0d63dc1',
        'spotifyUrl': 'http://abc.com'
      }, {
        'track': 'In tempor laborum voluptate enim occaecat Lorem eiusmod laborum enim.',
        'artist': 'Consectetur dolore ea sint qui irure.',
        'album': 'Non dolor eiusmod et ea nisi commodo ut do nulla.',
        'note': 'Amet pariatur cillum labore tempor consectetur qui tempor irure cillum ullamco qui dolore proident. Aute velit velit quis eiusmod aliqua.',
        'spotifyId': '5821602e578a84bbf8518719',
        'spotifyUrl': 'http://abc.com'
      }]
    }
  };

  const _data = {
    playlists: localStorageService.get(storageKey) || {}
  };

  function _getNextName (name) {
    let runner = 1;
    let newName = name;
    let sluggedName = Slug.slugify(name);

    if (getPlaylist(sluggedName)) {
      while (getPlaylist(`${sluggedName}-${runner}`)) {
        runner++;
      }

      newName += ' ' + runner;
      sluggedName += '-' + runner;
    }

    return {
      label: newName,
      slug: sluggedName
    };
  }

  function init () {
    Object.keys(seedData).forEach((currPlaylist) => {
      _data.playlists[currPlaylist] = seedData[currPlaylist];
    });

    _updateStoredPlaylist();
  }

  init();

  function _showToast (msg) {
    $mdToast.show(
      $mdToast.simple()
        .textContent(msg)
        .position(config.toast.position)
        .hideDelay(config.toast.timer)
    );
  }

  function _updateStoredPlaylist () {
    localStorageService.set(storageKey, _data.playlists);
  }

  return {
    getPlaylists,
    getPlaylistNames,
    getPlaylist,
    getPlaylistByName,
    removePlaylist,
    renamePlaylist,
    newPlaylist,
    addSong,
    editSong,
    removeSong
  };

  function getPlaylists () {
    return _data.playlists;
  }

  function getPlaylistNames () {
    return Object.keys(_data.playlists);
  }

  function getPlaylist (slug, exportable = false) {
    const playlist = _data.playlists[slug];
    if (!exportable) {
      return playlist;
    } else {
      const keysToDelete = ['spotifyUrl', 'spotifyId'];
      playlist.songs.forEach((currSong) => {
        keysToDelete.forEach((currKey) => {
          delete currSong[currKey];
        });
      });

      return {
        title: playlist.label,
        songs: playlist.songs
      };
    }
  }

  function getPlaylistByName (name) {
    const slug = Slug.slugify(name);
    return getPlaylist(slug);
  }

  function removePlaylist (slug, notify = true) {
    if (notify) {
      _showToast('Successfully deleted playlist');
    }
    delete _data.playlists[slug];
    _updateStoredPlaylist();
  }

  function renamePlaylist (oldName, newName) {
    const sluggedName = Slug.slugify(oldName);
    const oldPlaylist = getPlaylist(sluggedName);
    const newSafeName = _getNextName(newName);

    delete removePlaylist(sluggedName, false);

    oldPlaylist.label = newSafeName.label;
    oldPlaylist.slug = newSafeName.slug;
    _data.playlists[newSafeName.slug] = oldPlaylist;
    // update local storage
    _updateStoredPlaylist();
    return _data.playlists[newSafeName.slug];
  }

  function newPlaylist (name) {
    let newName = _getNextName(name);

    if (getPlaylist(newName.slug)) {
      _showToast('Failed to create playlist');
      return false;
    } else {
      _data.playlists[newName.slug] = {
        label: newName.label,
        slug: newName.slug,
        songs: [],
        createdAt: Date.now()
      };

      _updateStoredPlaylist();
      _showToast('Successfully created playlist');

      return _data.playlists[newName.slug];
    }
  }

  function addSong (playlistSlug, songObj) {
    const playlist = getPlaylist(playlistSlug);
    if (playlist) {
      const possibleDuplicates = _data.playlists[playlistSlug].songs.filter((curr) => {
        return curr.spotifyId === songObj.id;
      });

      if (!possibleDuplicates.length) {
        const newSongObj = {
          track: songObj.name,
          artist: songObj.artists[0].name,
          album: songObj.album.name,
          note: '',
          spotifyId: songObj.id,
          spotifyUrl: songObj.href
        };

        _data.playlists[playlistSlug].songs.push(newSongObj);
        _updateStoredPlaylist();
        _showToast(`Successfully added '${songObj.name}' to '${playlist.label}'`);

        return true;
      } else {
        _showToast(`You have already added '${songObj.name}' to '${playlist.label}'`);
        return false;
      }
    } else {
      _showToast('Could not add song because the playlist does not exist');
      return false;
    }
  }

  function editSong (playlistSlug, index, updatedSong) {
    _data.playlists[playlistSlug].songs[index] = updatedSong;
    _updateStoredPlaylist();
    _showToast('Successfully editted song');
    return updatedSong;
  }

  function removeSong (playlistSlug, index) {
    console.log('trying to remove song', index, ' from ', playlistSlug);
    _data.playlists[playlistSlug].songs.splice(index, 1);
    _updateStoredPlaylist();
    _showToast('Successfully removed song');
  }
};
