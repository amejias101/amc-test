require('angular/angular');

const detail = angular.module('detail', []);

detail.controller('DetailController', require('./detail.ctrl'));
detail.controller('EditSongController', require('./edit-song.ctrl'));

module.exports = detail;
