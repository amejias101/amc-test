module.exports = function (PlaylistsService, $stateParams, $mdDialog, $state, resolveData) {
  const self = this;

  self.currentPlaylist = resolveData;
  if (self.currentPlaylist) {
    const creationDate = new Date(self.currentPlaylist.createdAt);

    self.formattedDate = `${creationDate.getMonth() + 1}/${creationDate.getDate()}/${creationDate.getFullYear()}`;

    self.openMenu = function ($mdOpenMenu, ev) {
      $mdOpenMenu(ev);
    };

    self.renamePlaylist = function (ev) {
      const confirm = $mdDialog.prompt()
        .title('Enter the new name of your playlist')
        .placeholder('My Top 10 Songs')
        .initialValue(self.currentPlaylist.label)
        .targetEvent(event)
        .ok('Renew Playlist')
        .cancel('Cancel');

      $mdDialog.show(confirm).then(function (result) {
        const renamedPlaylist = PlaylistsService.renamePlaylist(self.currentPlaylist.label, result);
        $state.go('detail', {playlistId: renamedPlaylist.slug});
      });
    };

    self.deletePlaylist = function (ev) {
      const confirm = $mdDialog.confirm()
        .title('Are you sure that you would like delete this playlist?')
        .textContent(`You are about to delete the '${self.currentPlaylist.label}' playlist`)
        .targetEvent(ev)
        .ok('delete playlist!')
        .cancel('nevermind!');

      $mdDialog.show(confirm)
        .then(function () {
          PlaylistsService.removePlaylist($stateParams.playlistId);
          $state.go('detail', {playlistId: null});
        });
    };

    self.editSong = function (ev, song, songIndex) {
      // TODO WHY IS IT NOT SAVING CHANGES?!?!
      const currSong = song;
      self.currentEditSong = song;

      $mdDialog.show({
        controller: 'EditSongController',
        controllerAs: 'editSongCtrl',
        templateUrl: 'js/detail/edit-song.tmpl.html',
        parent: angular.element(document.body),
        locals: {currSong},
        targetEvent: ev
      })
      .then(function (answer) {
        if (answer === 'delete') {
          PlaylistsService.removeSong(self.currentPlaylist.slug, songIndex);
        } else {
          PlaylistsService.editSong(self.currentPlaylist.slug, songIndex, answer);
        }
      });
    };


    self.exportPlaylist = function () {
      const data = PlaylistsService.getPlaylist(self.currentPlaylist.slug, true);
      const json = JSON.stringify(data);
      const blob = new Blob([json], {type: 'application/json'});

      const event = document.createEvent('MouseEvents');
      const aTag = document.createElement('a');

      aTag.download = 'export.json';
      aTag.href = URL.createObjectURL(blob);
      aTag.dataset.downloadurl = ['text/json', aTag.download, aTag.href].join(':');
      // This is deprecated, update
      event.initEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
      aTag.dispatchEvent(event);
    };
  }
};
