module.exports = function ($scope, $mdDialog, currSong) {
  const self = this;

  $scope.currSong = angular.copy(currSong);

  self.cancel = function () {
    console.log('cancel');
    $mdDialog.cancel();
  };

  self.save = function () {
    currSong.note = angular.copy($scope.currSong.note);
    currSong.customImage = angular.copy($scope.currSong.customImage);
    $mdDialog.hide(currSong);
  };

  self.remove = function () {
    $mdDialog.hide('delete');
  };
};
