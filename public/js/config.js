module.exports = function config ($urlRouterProvider, $mdIconProvider) {
  $urlRouterProvider.otherwise('/');

  const iconBasePath = 'icons/';
  $mdIconProvider
    .icon('more', `${iconBasePath}ic_more_vert_white_24px.svg`, 24)
    .icon('check', `${iconBasePath}ic_check_white_24px.svg`, 24)
    .icon('delete-black', `${iconBasePath}ic_delete_black_24px.svg`, 24)
    .icon('add', `${iconBasePath}ic_add_white_24px.svg`, 24)
    .icon('add-black', `${iconBasePath}ic_add_black_24px.svg`, 24)
    .icon('edit', `${iconBasePath}ic_edit_white_24px.svg`, 24)
    .icon('edit-black', `${iconBasePath}ic_edit_black_24px.svg`, 24)
    .icon('close', `${iconBasePath}ic_close_white_24px.svg`, 24)
    .icon('search', `${iconBasePath}ic_search_white_24px.svg`, 24)
    .icon('close-black', `${iconBasePath}ic_close_black_24px.svg`, 24)
    .icon('save-black', `${iconBasePath}ic_save_black_24px.svg`, 24)
    .icon('audiotrack', `${iconBasePath}ic_audiotrack_white_24px.svg`, 24)
    .icon('audiotrack-black', `${iconBasePath}ic_audiotrack_black_24px.svg`, 24)
  ;
};
