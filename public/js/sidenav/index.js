require('angular/angular');

const sidenav = angular.module('sidenav', []);

sidenav.controller('sidenavController', require('./sidenav.ctrl'));
sidenav.component('sidenav', require('./sidenav.component'));

module.exports = sidenav;
