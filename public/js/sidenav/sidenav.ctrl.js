module.exports = function ($state, $mdDialog, PlaylistsService) {
  const self = this;

  self.addPlaylist = function (event) {
    const confirm = $mdDialog.prompt()
      .title('Enter the name of your new playlist')
      .placeholder('My Top 10 Songs')
      .targetEvent(event)
      .ok('Create New Playlist')
      .cancel('Cancel');

    $mdDialog.show(confirm).then(function (result) {
      const newPlaylist = PlaylistsService.newPlaylist(result);
      if (newPlaylist) {
        $state.go('detail', {playlistId: newPlaylist.slug});
      }
    });
  };

  self.goToSearch = function () {
    $state.go('search');
  };
};
