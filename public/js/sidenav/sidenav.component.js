module.exports = {
  // Binds the attibute data to the component controller.
  bindings: {
    data: '='
  },

  // We can now access the data from the data attribute with `$ctrl`
  templateUrl: 'js/sidenav/sidenav.tmpl.html',

  controller: 'sidenavController'
};
