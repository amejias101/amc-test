require('angular');
require('angular-aria');
require('angular-animate');
require('angular-material');
require('angular-material-icons');
require('angular-slugify');
require('angular-ui-router');
require('angular-hotkeys');
require('angular-local-storage');
require('angular-spotify');

const routes = require('./routes');
const config = require('./config');

require('./common');
require('./detail');
require('./search');
require('./sidenav');

const requires = [
  'ngMaterial',
  'ngMdIcons',
  'slugifier',
  'ui.router',
  'spotify',
  'ngAnimate',
  'LocalStorageModule',
  'cfp.hotkeys',
  'common',
  'detail',
  'search',
  'sidenav',
  'templates-main',
  'templates-icons'
];

window.app = angular.module('myApp', requires);

angular.module('myApp')
  .config(config)
  .config(routes);
