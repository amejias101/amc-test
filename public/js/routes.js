module.exports = function routes ($stateProvider) {
  $stateProvider
    .state('detail', {
      url: '/detail/{playlistId:string}',
      params: {
        playlistId: {value: null, squash: true}
      },
      resolve: {
        resolveData: function ($stateParams, PlaylistsService) {
          const p = new Promise((resolve, reject) => {
            const playlist = PlaylistsService.getPlaylist($stateParams.playlistId);

            if (playlist) {
              resolve(playlist);
            } else {
              resolve();
            }
          });

          return p.then(function (data) {
            return data;
          });
        }
      },
      templateUrl: 'js/detail/detail.tmpl.html',
      controller: 'DetailController as DetailCtrl'
    })
    .state('search', {
      url: '/search',
      templateUrl: 'js/search/search.tmpl.html',
      controller: 'SearchController as SearchCtrl'
    });
};
