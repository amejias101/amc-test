require('angular/angular');
const search = angular.module('search', []);

search.controller('SearchController', require('./search.ctrl'));

module.exports = search;
