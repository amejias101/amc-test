module.exports = function (Spotify, $timeout, PlaylistsService) {
  const self = this;

  self.searchQuery = 'against m';
  self.playlists = PlaylistsService.getPlaylists();

  self.openMenu = function ($mdOpenMenu, ev) {
    $mdOpenMenu(ev);
  };

  self.addToPlaylist = function (playlist, item) {
    console.log(playlist, item);
    PlaylistsService.addSong(playlist.slug, item);
  };

  self.onInputChanged = function () {
    const currentInput = self.searchQuery;

    $timeout(() => {
      if (currentInput === self.searchQuery) {
        if (currentInput) {
          Spotify.search(currentInput, 'track')
            .then((data) => {
              console.log('data from spotify', data);
              self.searchResults = data;
            });
        } else {
          delete self.searchResults;
        }
      }
    }, 1000);
  };
};
