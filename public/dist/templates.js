angular.module('templates-main', []).run(['$templateCache', function($templateCache) {
  $templateCache.put("js/detail/detail.tmpl.html",
    "<div flex>\n" +
    "  <div ng-if=\"DetailCtrl.currentPlaylist\">\n" +
    "    <md-toolbar layout=\"row\" class=\"md-hue-3\">\n" +
    "      <div class=\"md-toolbar-tools\">\n" +
    "        <span>{{DetailCtrl.currentPlaylist.label}}</span>\n" +
    "      </div>\n" +
    "      <span flex></span>\n" +
    "      <md-menu>\n" +
    "        <md-button aria-label=\"Open playlist interactions menu\" class=\"md-icon-button\" ng-click=\"DetailCtrl.openMenu($mdOpenMenu, $event)\">\n" +
    "           <md-icon md-svg-icon=\"more\" aria-label=\"more options\"></md-icon>\n" +
    "        </md-button>\n" +
    "        <md-menu-content width=\"4\">\n" +
    "          <md-menu-item>\n" +
    "            <md-button ng-click=\"DetailCtrl.deletePlaylist($event)\">\n" +
    "              <md-icon md-svg-icon=\"delete-black\" aria-label=\"more options\"></md-icon>\n" +
    "              Delete Playlist\n" +
    "            </md-button>\n" +
    "          </md-menu-item>\n" +
    "          <md-menu-item>\n" +
    "            <md-button ng-click=\"DetailCtrl.renamePlaylist($event)\">\n" +
    "              <md-icon md-svg-icon=\"edit-black\" aria-label=\"more options\"></md-icon>\n" +
    "              Rename Playlist\n" +
    "            </md-button>\n" +
    "          </md-menu-item>\n" +
    "          <md-menu-item>\n" +
    "            <md-button ng-click=\"DetailCtrl.exportPlaylist()\">\n" +
    "              <md-icon md-svg-icon=\"save-black\" aria-label=\"more options\"></md-icon>\n" +
    "              Export Playlist\n" +
    "            </md-button>\n" +
    "          </md-menu-item>\n" +
    "        </md-menu-content>\n" +
    "      </md-menu>\n" +
    "    </md-toolbar>\n" +
    "\n" +
    "    <md-content>\n" +
    "      <md-list flex>\n" +
    "        <md-subheader class=\"md-no-sticky\">{{DetailCtrl.currentPlaylist.songs.length}} Tracks - Created At {{DetailCtrl.formattedDate}}</md-subheader>\n" +
    "        <md-list-item class=\"md-3-line\" ng-repeat=\"song in DetailCtrl.currentPlaylist.songs track by song.track\" ng-click=\"null\" layout-padding>\n" +
    "          <md-icon md-svg-icon=\"audiotrack\" ng-if=\"!song.customImage\" ng-class=\"{'md-avatar-icon': !song.customImage}\"></md-icon>\n" +
    "          <img ng-src=\"{{song.customImage}}\" class=\"md-avatar\" alt=\"{{song.customImage}} image\" ng-if=\"song.customImage\"/>\n" +
    "          <div class=\"md-list-item-text\" layout=\"column\">\n" +
    "            <h3>{{ song.track }}</h3>\n" +
    "            <h4>{{ song.artist }}</h4>\n" +
    "            <p>{{ song.note }}</p>\n" +
    "          </div>\n" +
    "          <md-icon class=\"md-secondary\" ng-click=\"DetailCtrl.editSong($event, song, $index)\" md-svg-icon=\"edit-black\" aria-label=\"song.label\"></md-icon>\n" +
    "        </md-list-item>\n" +
    "      </md-list>\n" +
    "    </md-content>\n" +
    "  </div>\n" +
    "\n" +
    "  <div ng-if=\"!DetailCtrl.currentPlaylist\">\n" +
    "    <md-toolbar layout=\"row\" class=\"md-hue-3\">\n" +
    "      <div class=\"md-toolbar-tools\">\n" +
    "      </div>\n" +
    "      <span flex></span>\n" +
    "    </md-toolbar>\n" +
    "    <md-content>\n" +
    "      <h2>Please Select or Create a Playlist to Continue</h2>\n" +
    "    </md-content>\n" +
    "  </div>\n" +
    "</div>");
  $templateCache.put("js/detail/edit-song.tmpl.html",
    "<md-dialog>\n" +
    "  <form ng-cloak>\n" +
    "    <md-toolbar>\n" +
    "      <div class=\"md-toolbar-tools\">\n" +
    "        <h2>Edit</h2>\n" +
    "        <span flex></span>\n" +
    "        <md-button class=\"md-icon-button\" ng-click=\"editSongCtrl.cancel()\">\n" +
    "          <md-icon md-svg-icon=\"close-black\" aria-label=\"close dialog\"></md-icon>\n" +
    "        </md-button>\n" +
    "      </div>\n" +
    "    </md-toolbar>\n" +
    "\n" +
    "    <md-dialog-content>\n" +
    "      <div class=\"md-dialog-content\">\n" +
    "        <h2>{{editSongCtrl.currSong.track}}</h2>\n" +
    "      </div>\n" +
    "\n" +
    "      <md-input-container class=\"md-block\">\n" +
    "        <label>Image</label>\n" +
    "        <input ng-model=\"currSong.customImage\">\n" +
    "      </md-input-container>\n" +
    "\n" +
    "      <md-input-container class=\"md-block\">\n" +
    "        <label>Note</label>\n" +
    "        <textarea ng-model=\"currSong.note\" md-maxlength=\"140\" rows=\"3\" md-select-on-focus></textarea>\n" +
    "      </md-input-container>\n" +
    "\n" +
    "    </md-dialog-content>\n" +
    "\n" +
    "    <md-dialog-actions layout=\"row\">\n" +
    "      <md-button ng-click=\"editSongCtrl.remove()\">\n" +
    "        Remove\n" +
    "      </md-button>\n" +
    "      <span flex></span>\n" +
    "      <md-button ng-click=\"editSongCtrl.cancel()\">\n" +
    "       Cancel\n" +
    "      </md-button>\n" +
    "      <md-button ng-click=\"editSongCtrl.save(editSongCtrl.currentEditSong)\">\n" +
    "        Save\n" +
    "      </md-button>\n" +
    "    </md-dialog-actions>\n" +
    "  </form>\n" +
    "</md-dialog>\n" +
    "\n" +
    "");
  $templateCache.put("js/search/search.tmpl.html",
    "<md-content class=\"search-results\">\n" +
    "    <md-toolbar class=\"md-warn\">\n" +
    "    <div class=\"md-toolbar-tools\">\n" +
    "      <h2 class=\"md-flex\">Add Songs to your Playlists</h2>\n" +
    "    </div>\n" +
    "  </md-toolbar>\n" +
    "  <md-input-container>\n" +
    "    <label>Search</label>\n" +
    "    <input ng-model=\"SearchCtrl.searchQuery\" ng-change=\"SearchCtrl.onInputChanged()\">\n" +
    "  </md-input-container>\n" +
    "  <md-content>\n" +
    "    <md-list>\n" +
    "      <md-list-item class=\"md-2-line\" ng-click=\"null\" ng-repeat=\"item in SearchCtrl.searchResults.tracks.items\">\n" +
    "        <div class=\"md-list-item-text\">\n" +
    "          <h3>{{item.name}}</h3>\n" +
    "          <h4>{{item.artists[0].name}}</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <md-menu>\n" +
    "          <md-button aria-label=\"Open playlist interactions menu\" class=\"md-icon-button\" ng-click=\"SearchCtrl.openMenu($mdOpenMenu, $event)\">\n" +
    "            <md-icon md-svg-icon=\"add-black\" aria-label=\"more options\"></md-icon>\n" +
    "          </md-button>\n" +
    "          <md-menu-content width=\"4\">\n" +
    "            <md-menu-item ng-repeat=\"playlist in SearchCtrl.playlists\">\n" +
    "              <md-button ng-click=\"SearchCtrl.addToPlaylist(playlist, item)\">\n" +
    "                {{playlist.label}}\n" +
    "              </md-button>\n" +
    "            </md-menu-item>\n" +
    "          </md-menu-content>\n" +
    "        </md-menu>\n" +
    "      </md-list-item>\n" +
    "    </md-list>\n" +
    "  </md-content>\n" +
    "</md-content>\n" +
    "");
  $templateCache.put("js/sidenav/sidenav.tmpl.html",
    "<md-content>\n" +
    "  <md-toolbar>\n" +
    "    <div class=\"md-toolbar-tools\">\n" +
    "      <h2>\n" +
    "        <span>My Playlists</span>\n" +
    "      </h2>\n" +
    "      <span flex></span>\n" +
    "      <md-button class=\"md-icon-button\" aria-label=\"add playlist\" ng-click=\"$ctrl.addPlaylist($event)\">\n" +
    "        <md-icon md-svg-icon=\"add\" aria-label=\"add playlist\"></md-icon>\n" +
    "      </md-button>\n" +
    "      <md-button class=\"md-icon-button\" aria-label=\"Search Songs\" ng-click=\"$ctrl.goToSearch()\">\n" +
    "        <md-icon md-svg-icon=\"search\"></md-icon>\n" +
    "      </md-button>\n" +
    "    </div>\n" +
    "  </md-toolbar>\n" +
    "  <md-content layout-padding>\n" +
    "    <md-list flex>\n" +
    "      <!--<md-list-item ng-repeat=\"item in app.menu\" ui-sref=\"{{item.state}}\" ui-sref-active=\"item-menu-active\">-->\n" +
    "      <md-list-item class=\"md-2-line\" ui-sref=\"detail({ playlistId: key })\" ui-sref-active=\"item-menu-active\" ng-repeat=\"(key, playlist) in $ctrl.data track by $index\">\n" +
    "        <div class=\"md-list-item-text\" layout=\"column\">\n" +
    "          <h3>{{playlist.label}}</h3>\n" +
    "          <p>{{playlist.songs.length}} Songs</p>\n" +
    "        </div>\n" +
    "      </md-list-item>\n" +
    "    </md-list>\n" +
    "\n" +
    "  </md-content>\n" +
    "<md-content>");
}]);
