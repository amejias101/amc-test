angular.module('templates-icons', []).run(['$templateCache', function($templateCache) {
  $templateCache.put("icons/ic_add_black_24px.svg",
    "<svg fill=\"#000000\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "    <path d=\"M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z\"/>\n" +
    "    <path d=\"M0 0h24v24H0z\" fill=\"none\"/>\n" +
    "</svg>");
  $templateCache.put("icons/ic_add_white_24px.svg",
    "<svg fill=\"#FFFFFF\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "    <path d=\"M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z\"/>\n" +
    "    <path d=\"M0 0h24v24H0z\" fill=\"none\"/>\n" +
    "</svg>");
  $templateCache.put("icons/ic_audiotrack_black_24px.svg",
    "<svg fill=\"#000000\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "    <path d=\"M0 0h24v24H0z\" fill=\"none\"/>\n" +
    "    <path d=\"M12 3v9.28c-.47-.17-.97-.28-1.5-.28C8.01 12 6 14.01 6 16.5S8.01 21 10.5 21c2.31 0 4.2-1.75 4.45-4H15V6h4V3h-7z\"/>\n" +
    "</svg>");
  $templateCache.put("icons/ic_audiotrack_white_24px.svg",
    "<svg fill=\"#FFFFFF\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "    <path d=\"M0 0h24v24H0z\" fill=\"none\"/>\n" +
    "    <path d=\"M12 3v9.28c-.47-.17-.97-.28-1.5-.28C8.01 12 6 14.01 6 16.5S8.01 21 10.5 21c2.31 0 4.2-1.75 4.45-4H15V6h4V3h-7z\"/>\n" +
    "</svg>");
  $templateCache.put("icons/ic_bookmark_border_white_24px.svg",
    "<svg fill=\"#FFFFFF\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "    <path d=\"M17 3H7c-1.1 0-1.99.9-1.99 2L5 21l7-3 7 3V5c0-1.1-.9-2-2-2zm0 15l-5-2.18L7 18V5h10v13z\"/>\n" +
    "    <path d=\"M0 0h24v24H0z\" fill=\"none\"/>\n" +
    "</svg>");
  $templateCache.put("icons/ic_cancel_white_24px.svg",
    "<svg fill=\"#FFFFFF\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "    <path d=\"M12 2C6.47 2 2 6.47 2 12s4.47 10 10 10 10-4.47 10-10S17.53 2 12 2zm5 13.59L15.59 17 12 13.41 8.41 17 7 15.59 10.59 12 7 8.41 8.41 7 12 10.59 15.59 7 17 8.41 13.41 12 17 15.59z\"/>\n" +
    "    <path d=\"M0 0h24v24H0z\" fill=\"none\"/>\n" +
    "</svg>");
  $templateCache.put("icons/ic_check_white_24px.svg",
    "<svg fill=\"#FFFFFF\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "    <path d=\"M0 0h24v24H0z\" fill=\"none\"/>\n" +
    "    <path d=\"M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z\"/>\n" +
    "</svg>");
  $templateCache.put("icons/ic_close_black_24px.svg",
    "<svg fill=\"#000000\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "    <path d=\"M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z\"/>\n" +
    "    <path d=\"M0 0h24v24H0z\" fill=\"none\"/>\n" +
    "</svg>");
  $templateCache.put("icons/ic_close_white_24px.svg",
    "<svg fill=\"#FFFFFF\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "    <path d=\"M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z\"/>\n" +
    "    <path d=\"M0 0h24v24H0z\" fill=\"none\"/>\n" +
    "</svg>");
  $templateCache.put("icons/ic_delete_black_24px.svg",
    "<svg fill=\"#000000\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "    <path d=\"M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z\"/>\n" +
    "    <path d=\"M0 0h24v24H0z\" fill=\"none\"/>\n" +
    "</svg>");
  $templateCache.put("icons/ic_delete_white_24px.svg",
    "<svg fill=\"#FFFFFF\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "    <path d=\"M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z\"/>\n" +
    "    <path d=\"M0 0h24v24H0z\" fill=\"none\"/>\n" +
    "</svg>");
  $templateCache.put("icons/ic_edit_black_24px.svg",
    "<svg fill=\"#000000\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "    <path d=\"M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z\"/>\n" +
    "    <path d=\"M0 0h24v24H0z\" fill=\"none\"/>\n" +
    "</svg>");
  $templateCache.put("icons/ic_edit_white_24px.svg",
    "<svg fill=\"#FFFFFF\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "    <path d=\"M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z\"/>\n" +
    "    <path d=\"M0 0h24v24H0z\" fill=\"none\"/>\n" +
    "</svg>");
  $templateCache.put("icons/ic_link_white_24px.svg",
    "<svg fill=\"#FFFFFF\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "    <path d=\"M0 0h24v24H0z\" fill=\"none\"/>\n" +
    "    <path d=\"M3.9 12c0-1.71 1.39-3.1 3.1-3.1h4V7H7c-2.76 0-5 2.24-5 5s2.24 5 5 5h4v-1.9H7c-1.71 0-3.1-1.39-3.1-3.1zM8 13h8v-2H8v2zm9-6h-4v1.9h4c1.71 0 3.1 1.39 3.1 3.1s-1.39 3.1-3.1 3.1h-4V17h4c2.76 0 5-2.24 5-5s-2.24-5-5-5z\"/>\n" +
    "</svg>");
  $templateCache.put("icons/ic_more_vert_white_24px.svg",
    "<svg fill=\"#FFFFFF\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "    <path d=\"M0 0h24v24H0z\" fill=\"none\"/>\n" +
    "    <path d=\"M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z\"/>\n" +
    "</svg>");
  $templateCache.put("icons/ic_save_black_24px.svg",
    "<svg fill=\"#000000\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "    <path d=\"M0 0h24v24H0z\" fill=\"none\"/>\n" +
    "    <path d=\"M17 3H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V7l-4-4zm-5 16c-1.66 0-3-1.34-3-3s1.34-3 3-3 3 1.34 3 3-1.34 3-3 3zm3-10H5V5h10v4z\"/>\n" +
    "</svg>");
  $templateCache.put("icons/ic_search_white_24px.svg",
    "<svg fill=\"#FFFFFF\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "    <path d=\"M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z\"/>\n" +
    "    <path d=\"M0 0h24v24H0z\" fill=\"none\"/>\n" +
    "</svg>");
  $templateCache.put("icons/ic_sort_by_alpha_white_24px.svg",
    "<svg fill=\"#FFFFFF\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "    <path d=\"M0 0h24v24H0V0zm0 0h24v24H0V0zm.75.75h22.5v22.5H.75z\" fill=\"none\"/>\n" +
    "    <path d=\"M14.94 4.66h-4.72l2.36-2.36zm-4.69 14.71h4.66l-2.33 2.33zM6.1 6.27L1.6 17.73h1.84l.92-2.45h5.11l.92 2.45h1.84L7.74 6.27H6.1zm-1.13 7.37l1.94-5.18 1.94 5.18H4.97zm10.76 2.5h6.12v1.59h-8.53v-1.29l5.92-8.56h-5.88v-1.6h8.3v1.26l-5.93 8.6z\"/>\n" +
    "</svg>");
}]);
