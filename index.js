const express = require('express');
const path = require('path');

const app = express();

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

app.get('/partials/:name', function (req, res) {
  const name = req.params.name;
  res.sendFile(path.join(__dirname, `views/partials/${name}.html`));
});

const port = 8086;
// Start server
app.listen(port, function () {
  console.log(`Express server listening on port ${port}, http://localhost:${port}`);
});
